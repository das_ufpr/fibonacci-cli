#!/usr/bin/env node

const yargs = require('yargs');
const Fibonacci = require('./fibonacci.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <n-esimo termo>")
   .option("n", { alias: "n-esimo", describe: "n-ésimo número da sequência de Fibonacci", type: "string", demandOption: true })
   .argv;

  const rn = new Fibonacci(options.n);
    
  console.log(rn.fibonacciSeries());
}

//Para rodar:
// $ node bin/fibonacci -n 5

// Ou
// $ npm install -g .
// $ fibonacci -n 5