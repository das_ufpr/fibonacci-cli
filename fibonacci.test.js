import Fibonacci from "./fibonacci";

describe('Fibonacci', () => {
  describe('Units', () => {
    
    test('should return a 0', () => {
        const fibonacci = new Fibonacci(1);
        expect(fibonacci.fibonacciSeries()).toEqual(0);
    });

    test('should return 1', () => {
        const fibonacci = new Fibonacci(2);
        expect(fibonacci.fibonacciSeries()).toEqual(1);
    });

    test('should return 3', () => {
        const fibonacci = new Fibonacci(5);
        expect(fibonacci.fibonacciSeries()).toEqual(3);
    });

    test('should return 34', () => {
        const fibonacci = new Fibonacci(10);
        expect(fibonacci.fibonacciSeries()).toEqual(34);
    });

    test('should return 4181', () => {
        const fibonacci = new Fibonacci(20);
        expect(fibonacci.fibonacciSeries()).toEqual(4181);
    });

  });
});