class Fibonacci {
    
    constructor(n) {
        this.n = n;
    }

    fibonacciSeries() {
        let series = [];
        let element;
        for (let i = 1; i <= this.n; i++) {
            if(i == 1) {
                element = 0;
                series.push(element);
            }
            else if(i == 2) {
                element = 1;
                series.push(element);
            } else {
                element = series[i-2] + series[i-3];
                series.push(element);
            }
        }
        return series[series.length - 1];
    }

}

module.exports = Fibonacci;

//const fibonacci = new Fibonacci(10);
//console.log(fibonacci.fibonacciSeries());
//console.log(typeof fibonacci.fibonacciSeries())